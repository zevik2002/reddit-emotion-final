import axios from 'axios';
import { config } from 'dotenv';
import { CustomHttpException } from 'src/error-handling/custom-error-handl';
import { redditToken } from './redditToken';
config();

const REDDIT_API = process.env.REDDIT_API || 'https://oauth.reddit.com';

export const fetchRedditAPI = async (urlExtension: string = '') => {
  try {
    if (!redditToken) throw new CustomHttpException('no token found !', 500);
    const url = REDDIT_API + urlExtension;
    axios.defaults.headers.common.Authorization = `bearer ${redditToken}`;
    const { data: category } = await axios.get(url);
    return category;
  } catch (error) {
    return Promise.reject(error);
  }
};
