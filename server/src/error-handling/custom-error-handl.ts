import { HttpException, HttpStatus } from '@nestjs/common';

export class CustomHttpException extends HttpException {
  constructor(message: string, status: HttpStatus, customData?: string) {
    super({ message, customData: customData }, status);
  }
}

