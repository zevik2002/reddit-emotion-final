import { IsLoginGuard } from './is-login.guard';
import { Test, TestingModule } from '@nestjs/testing';

describe('IsLoginGuard', () => {
  let guard: IsLoginGuard;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IsLoginGuard],
    }).compile();

    guard = module.get<IsLoginGuard>(IsLoginGuard);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });

  it('should return false if no token is provided', async () => {
    expect(await guard.canActivate({} as any)).toBeFalsy();
  });

  it('should return false if invalid token is provided', async () => {
    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            'x-auth-token': 'invalid',
          },
        }),
      }),
    } as any;
    expect(await guard.canActivate(context)).toBeFalsy();
  });

  it('should return true if valid token is provided', async () => {
    jest
      .spyOn(require('../../helpers/jwt'), 'verifyToken')
      .mockReturnValueOnce({
        id: 1,
        username: 'test',
      });

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            'x-auth-token': 'valid',
          },
        }),
      }),
    } as any;
    expect(await guard.canActivate(context)).toBeTruthy();
  });
});
