import { sign, verify } from 'jsonwebtoken';
import { TokenInterface } from '../interfaces/TokenInterface';
import { config } from 'dotenv';
config();

const secret_jwt = process.env.SECRET_KEY;

export const generateToken = (user: TokenInterface) => sign(user, secret_jwt);

export const verifyToken = (tokenFromClient: string) =>
  verify(tokenFromClient, secret_jwt);
