import { NameDto } from 'src/users/dto/user.dto';

export interface TokenInterface {
  _id: number;
  isAdmin: boolean;
  iat?: number;
  name: NameDto;
}

export interface UserPayloadInterface extends Request {
  user: TokenInterface;
}
