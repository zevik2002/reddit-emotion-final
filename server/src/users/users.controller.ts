import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UnauthorizedException,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { SignUpDto } from './dto/signUp.dto';
import { UserRequestDto } from './dto/user.dto';
import { IsLoginGuard } from 'src/auth/guards/is-login/is-login.guard';
import { UserPayloadInterface } from 'src/auth/interfaces/TokenInterface';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('login')
  @UsePipes()
  async login(@Body(new ValidationPipe()) userRequestDto: UserRequestDto) {
    try {
      const Token = await this.usersService.login(userRequestDto);

      return Token;
    } catch (error) {
      throw new UnauthorizedException('password or email are incorrect');
    }
  }

  @UseGuards(IsLoginGuard)
  @Get('history')
  async history(@Req() req: UserPayloadInterface) {
    const userId = req.user._id;
    try {
      const history = await this.usersService.getHistory(userId);
      return history;
    } catch (error) {
      throw new Error('history not found');
    }
  }

  @Post('signup')
  @UsePipes()
  async signUp(@Body(new ValidationPipe()) signUpDto: SignUpDto) {
    try {
      return await this.usersService.signUp(signUpDto);
    } catch (error) {
      throw new UnauthorizedException('password or email are incorrect');
    }
  }
}
