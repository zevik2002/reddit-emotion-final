export interface PostsByCategory {
  data: {
    id: string;
    title: string;
    url_overridden_by_dest?: string;
    thumbnail: string;
    subreddit: string;
    selftext: string;
    author: string;
    created: number;
  };
}

interface ImageData {
  source: {
    url: string;
  };
}

export interface CategoryDataObj {
  title: string;
  name: string;
  display_name: string;
}

export interface Categories {
  data: CategoryDataObj;
}
