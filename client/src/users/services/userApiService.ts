import axios from "axios";
import { UserFormInput } from "../../global/types/types";
import { getErrorMessage } from "../../global/utils/algoMethods";

const BASE_URL = import.meta.env.VITE_BASE_URL || "http://localhost:8080";

export const login = async (email: string, password: string) => {
  try {
    const { data } = await axios.post(` ${BASE_URL}/users/login `, {
      email: email,
      password: password,
    });

    return data;
  } catch (error) {
    return Promise.reject(getErrorMessage(error));
  }
};

export const signUp = async (newUser: UserFormInput) => {
  const data = await axios.post(`${BASE_URL}/users/signup`, {
    name: { first: newUser.name?.first, last: newUser.name?.last },
    email: newUser.email,
    password: newUser.password,
  });
  return data;
};

