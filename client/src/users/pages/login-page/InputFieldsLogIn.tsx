import { FormControl } from "@mui/material";
import InputEmail from "../../../global/components/formInput/InputEmail";
import InputPassword from "../../../global/components/formInput/InputPassowrd";
import { FromProps } from "../../../global/components/formInput/propsType";

const InputFieldsLogIn = (props: FromProps) => {
    return (
        <FormControl fullWidth>
          <InputEmail register={props.register} formState={props.formState} trigger={props.trigger} />
          <InputPassword register={props.register} formState={props.formState} trigger={props.trigger} />
         </FormControl>
    )
}
export default InputFieldsLogIn;