import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import PostInterface from "../../../../posts/interfaces/PostInterface";
import { parseDateString } from "../../../../global/utils/algoMethods";
import { useNavigate } from "react-router-dom";

type TableProps = {
  posts: PostInterface[];
};
export const UserHistoryPostsTable = ({ posts }: TableProps) => {
  const navigate = useNavigate();
  return (
    <Box sx={{ margin: "5px", width: "98%", height: "98%" }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>title</TableCell>
            <TableCell>author</TableCell>
            <TableCell>creation date</TableCell>
            <TableCell>sentiment</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {posts.map((post) => {
            return (
              <TableRow key={post._id}>
                <TableCell
                  onClick={() =>
                    navigate(`/post-details/${post.category}/${post._id}`)
                  }
                  sx={{cursor:'pointer'}}
                >
                  {post.title}
                </TableCell>
                <TableCell>{post.author}</TableCell>
                <TableCell>{parseDateString(new Date(post.created))}</TableCell>
                <TableCell>{post.sentiment}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Box>
  );
};
