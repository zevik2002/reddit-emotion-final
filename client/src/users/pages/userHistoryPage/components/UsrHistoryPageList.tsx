import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import RedditIcon from "@mui/icons-material/Reddit";
import { ListItemAvatar, Avatar } from "@mui/material";
import PostInterface from "../../../../posts/interfaces/PostInterface";
import { useNavigate } from "react-router-dom";

export default function UserHistoryPageList({
  posts,
}: {
  posts: PostInterface[];
}) {
  const navigate = useNavigate();

  return (
    <List
      sx={{ width: "100%", bgcolor: "background.paper", marginBottom: "5px" }}
    >
      {posts.map((p) => (
        <ListItem key={p._id} onClick={() =>
          navigate(`/post-details/${p.category}/${p._id}`)
        }
        sx={{cursor:'pointer'}}>
          <ListItemAvatar>
            <Avatar>
              <RedditIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText key={p._id} primary={p.title} secondary={p.author} />
        </ListItem>
      ))}
    </List>
  );
}
