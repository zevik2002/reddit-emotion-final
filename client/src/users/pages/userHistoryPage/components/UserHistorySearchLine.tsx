/* eslint-disable @typescript-eslint/ban-types */
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { useNavigate } from "react-router-dom";
import PostInterface from "../../../../posts/interfaces/PostInterface";

export default function UserHistorySearchLine({
  data,
}: {
  data: PostInterface[];
}) {
  const options = data.map((option) => {
    const firstLetter = option.title[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? "0-9" : firstLetter,
      ...option,
    };
  });
  const navigate = useNavigate();

  const handleChange = (
    _event: React.ChangeEvent<{}>,
    value: null | PostInterface
  ) => {
    if (value) {
      navigate(`/post-details/${value.category}/${value._id}`);
    }
  };

  return (
    <Autocomplete
      id="grouped-demo"
      options={options.sort(
        (a, b) => -b.firstLetter.localeCompare(a.firstLetter)
      )}
      groupBy={(option) => option.firstLetter}
      getOptionLabel={(option) => option.title}
      onChange={handleChange}
      sx={{ width: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Search for Post" />
      )}
    />
  );
}
