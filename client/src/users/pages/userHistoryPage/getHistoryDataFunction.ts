export default function getHistoryData() {
  return [
    {
      title: "The Art of War",
      author: "Sun Tzu",
      category: "military_strategy",
      imageUrl: "",
      created: new Date(),
      sentiment: "NEGATIVE",
      selftext:
        "The Art of War is an ancient Chinese military treatise attributed to Sun Tzu, a high-ranking military general, strategist, and tactician.",
      _id: "1",
    },
    {
      title: "To Kill a Mockingbird",
      author: "Harper Lee",
      selftext:
        "To Kill a Mockingbird is a novel by Harper Lee published in 1960. It was immediately successful, winning the Pulitzer Prize, and has become a classic of modern American literature.",
      _id: "2",
      category: "literature",
      imageUrl: "",
      created: new Date(),
      sentiment: "NEGATIVE",
    },
    {
      title: "The Hitchhiker's Gu_ide to the Galaxy",
      author: "Douglas Adams",
      selftext:
        "The Hitchhiker's Gu_ide to the Galaxy is a comedy science fiction series created by Douglas Adams. The series follows the misadventures of Arthur Dent, an unwitting human who is plucked off Earth seconds before it is destroyed.",
      _id: "3",
      category: "science_fiction",
      imageUrl: "",
      created: new Date(),
      sentiment: "NEGATIVE",
    },
    {
      title: "The Lord of the Ring",
      author: "J.R.R. Tolkien",
      category: "Fantasy",
      imageUrl: "",
      created: new Date(),
      sentiment: "POSITIVE",
      selftext: "best book ever",
      _id: "4",
    },
    {
      title: "A tell of love and dark",
      author: "Amos oz",
      category: "Biography",
      imageUrl: "",
      created: new Date(),
      sentiment: "NEUTRAL",
      selftext: "weird one",
      _id: "5",
    },
  ];
}
