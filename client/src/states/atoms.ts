import { atom } from "jotai";
import { ErrorPageProps } from "../global/types/types";

interface UserNameInterface {
  firstName: string;
  lastName: string;
}

export const userName = atom<UserNameInterface>({ firstName: "jon", lastName: "doe" });
export const errorPagePropsAtom = atom<ErrorPageProps>({ status: 404 });
export const userFirstNameAtom = atom(localStorage.getItem("firstName") || "john" || null);
export const userLastNameAtom = atom(localStorage.getItem("lastName") || "doe" || null);
export const userEmailAtom = atom(localStorage.getItem("email") || null);
export const tokenAtom = atom(localStorage.getItem("token") || null);
export const userIdAtom = atom(localStorage.getItem("usetrId") || null);
//2024>>2023