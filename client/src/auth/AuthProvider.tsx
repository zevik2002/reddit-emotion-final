import {
  useState,
  useContext,
  ReactNode,
  SetStateAction,
  Dispatch,
  FC,
  createContext,
  useEffect,
} from "react";
import { jwtDecode } from "jwt-decode";
import TokenPayloadInterface from "./TokenPayloadInterface";

type ContextValue = {
  token: string | null;
  setToken: Dispatch<SetStateAction<string | null>>;
  user: null | TokenPayloadInterface;
};

const AuthContext = createContext<null | ContextValue>(null);
const { Provider } = AuthContext;

type AuthProviderProps = {
  children: ReactNode;
};

const AuthProvider: FC<AuthProviderProps> = ({ children }) => {
  const [token, setToken] = useState<string | null>(() =>
    localStorage.getItem("token")
  );
  const [user, setUser] = useState<null | TokenPayloadInterface>(null);

  useEffect(() => {
    if (!token) setUser(null);
    if (token) {
      localStorage.setItem("token", token);
      const userFromToken: TokenPayloadInterface = jwtDecode(token);
      setUser(userFromToken);
    }
  }, [token]);

  return <Provider value={{ token, setToken, user }}>{children}</Provider>;
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) throw new Error("useAuth must be used within a AuthProvider");
  return context;
};

export default AuthProvider;
//BBSARAH
