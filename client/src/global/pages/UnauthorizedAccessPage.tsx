import { Box, Typography } from "@mui/material";
import NavLink from "../router/components/NavLink";
import ROUTES from "../router/routes-model";
import { theme } from "../styles/themes/themeModels";

const UnauthorizedAccessPage = () => {
  return (
    <Box
      sx={{
        textAlign: "center",
        mt: 8,
      }}
    >
      <Typography variant="h3" component="h1" gutterBottom color="warning.main">
        Access Denied
      </Typography>
      <Typography variant="h6" color="">
        You are not authorized to access this page.
      <Box mt={1}> 
        <NavLink to={ROUTES.LOGIN} color={theme.palette.info.main}>
          Please log in to continue.
        </NavLink>
      </Box>
      </Typography>
    </Box>
  );
};

export default UnauthorizedAccessPage;
