import { Box, Typography } from "@mui/material";
import NavLink from "../router/components/NavLink";
import { theme } from "../styles/themes/themeModels";
const Error404Page = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          mt: 2,
          minHeight: "100vh",
        }}
      >
        <Typography variant="h3" fontWeight="bold" m={4}>
          Page Not Found
        </Typography>

        <Typography
          variant="h5"
          color="primary"
          style={{ marginBottom: "10px" }}
        >
          The page you’re looking for doesn’t exist.
        </Typography>
        <NavLink
          to="/"
          color={theme.palette.info.main}
          sx={{ marginBottom: "24px" }}
        >
          Back Home...
        </NavLink>
        <img src="../../../images/404-page.webp" alt="404" width={600} />
      </Box>
    </Box>
  );
};
export default Error404Page;
