import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import HomePage from "./HomePage";
describe("renders HomePage compnent", () => {
  test("renders HomePage checks title and subtitle", () => {
    render(
      <BrowserRouter>
        <HomePage />
      </BrowserRouter>
    );

    const titleElement = screen.getByText(
      "Reddit Emotions Site"
    ) as HTMLElement;
    expect(titleElement).toBeTruthy();

    const subtitleElement = screen.getByText(/Reddit is home/) as HTMLElement;
    expect(subtitleElement).toBeTruthy();
  });

  test("renders 'For More Details...' link", () => {
    render(
      <BrowserRouter>
        <HomePage />
      </BrowserRouter>
    );

    const linkElement = screen.getByText(
      "For More Details..."
    ) as HTMLAnchorElement;
    expect(linkElement).toBeTruthy();
    expect(linkElement.href).toBe("http://localhost:3000/select-post");
    expect(linkElement.target).toBe("");
  });

  test("renders image with alt text 'Reddit Emotions Site'", () => {
    render(
      <BrowserRouter>
        <HomePage />
      </BrowserRouter>
    );

    const imageElement = screen.getByAltText(
      "Reddit Emotions Site"
    ) as HTMLImageElement;
    expect(imageElement).toBeTruthy();
    expect(imageElement.src).toBe(
      "http://localhost:3000/images/reddit-homePage-pic.webp"
    );
  });
});
