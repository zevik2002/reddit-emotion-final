import { Container } from "@mui/system";
import CenterNavigation from "./center-navigation/CenterNavigation";
import LeftNavigation from "./left-navigation/LeftNavigation";
import RightNavigation from "./right-navigation/RightNavigation";
import { AppBar, Toolbar, Grid, Button } from "@mui/material";
import { theme } from "../../styles/themes/themeModels";
import ROUTES from "../../router/routes-model";
import NavLink from "../../router/components/NavLink";
import LogOutNavigation from "./log-out/LogOutnavigation";
import { useAuth } from "../../../auth/AuthProvider";

const Header = () => {
  const { user } = useAuth();

  return (
    <AppBar
      position="sticky"
      sx={{ backgroundColor: theme.palette.primary.main }}
    >
      <Container>
        <Toolbar>
          <Grid
            container
            justifyContent="space-between"
            alignItems="center"
            spacing={2}
          >
            <Grid item>
              <LeftNavigation />
            </Grid>
            {user && (
              <Grid item>
                <Button sx={{ color: "white" }}>
                  <NavLink to={ROUTES.POSTS_HISTORY}>
                    {" "}
                    <CenterNavigation />
                  </NavLink>
                </Button>
              </Grid>
            )}

            {user && <LogOutNavigation />}

            {!user && (
              <Grid item>
                <RightNavigation />
              </Grid>
            )}
          </Grid>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;
