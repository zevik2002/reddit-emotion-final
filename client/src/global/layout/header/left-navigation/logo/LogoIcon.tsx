import { IconButton } from "@mui/material";
import { Box } from "@mui/system";

const LogoIcon = () => {
  return (
    <Box>
      <IconButton edge="start" color="inherit" aria-label="menu">
        <img
          src="https://www.iconpacks.net/icons/2/free-reddit-logo-icon-2436-thumb.png"
          alt="Reddit Logo"
          style={{ width: "30px", height: "auto" }}
        />
      </IconButton>
    </Box>
  );
};

export default LogoIcon;
