import { Grid, Typography } from "@mui/material";
import { CSSProperties, FC, ReactNode } from "react";

type HeaderLinkProps = {
  children: ReactNode;
  sx?: CSSProperties;
};

const HeaderLink: FC<HeaderLinkProps> = ({ children, sx }) => {
  return (
    <Grid item>
      <Typography sx={sx}>{children}</Typography>
    </Grid>
  );
};

export default HeaderLink;
