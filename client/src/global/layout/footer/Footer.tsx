import { Container, Grid } from "@mui/material";
import FooterLink from "./footer-link/FooterLink";
import { Box } from "@mui/system";
import NavLink from "../../router/components/NavLink";
import { theme } from "../../styles/themes/themeModels";
import Logo from "../logo/Logo";
import LogoIcon from "../logo/LogoIcon";

const Footer = () => {
  return (
    <Box sx={{ backgroundColor: theme.palette.primary.main }}>
      <Container>
        <Grid container justifyContent={"center"} mt={1}>
          <Grid item display={"flex"} gap={"5px"}>
            <NavLink
              to="/"
              sx={{
                display: "flex",
                flexDirection: "row-reverse",
                color: "white",
              }}
            >
              <Logo sx={{ display: "flex" }} />
              <LogoIcon />
            </NavLink>
          </Grid>
        </Grid>

        <Grid container justifyContent="space-between" py={1}>
          <FooterLink>+972-5005858</FooterLink>
          <FooterLink>Created By: Reddit Emotion Itt</FooterLink>
          <FooterLink>reddit@gmail.com</FooterLink>
        </Grid>
      </Container>
    </Box>
  );
};

export default Footer;
