import axios from "axios";
import { useEffect } from "react";
import { useAuth } from "../../auth/AuthProvider";

const useAxiosInterceptors = () => {
  const { token } = useAuth();

  useEffect(() => {
    axios.defaults.headers.common["x-auth-token"] = token;
    axios.interceptors.request.use((data) => {
      return Promise.resolve(data);
    }, null);
  }, [token]);
};

export default useAxiosInterceptors;
