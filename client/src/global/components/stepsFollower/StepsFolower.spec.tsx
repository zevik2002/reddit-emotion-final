import { render, screen } from "@testing-library/react";
import StepsFollower from "./StepsFollower";

describe("Testing StepsFollower component", () => {
  it("renders steps labels correctly", () => {
    const stepsArray: string[] = [
      "Select category",
      "Select Sub Category",
      "Filter Posts",
    ];
    const stepIndex: number = 0;

    render(<StepsFollower stepIndex={stepIndex} stepsArray={stepsArray} />);

    stepsArray.forEach((label) => {
      const elementWithText = screen.getByText(label);
      expect(elementWithText).toBeTruthy();
    });
  });

  //   it("renders active step correctly", () => {
  //     const stepsArray: string[] = [
  //       "Select category",
  //       "Select Sub Category",
  //       "Filter Posts",
  //     ];
  //     const stepIndex: number = 1;

  //     render(<StepsFollower stepIndex={stepIndex} stepsArray={stepsArray} />);

  //     const activeStep = screen.getByText(stepsArray[stepIndex]);
  //     expect(activeStep).toHaveProperty("aria-hidden", "false");
  //   });
});
