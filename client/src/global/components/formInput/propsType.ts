import { FieldErrors, UseFormRegister, UseFormTrigger } from "react-hook-form";
import { UserFormInput } from "../../types/types";

export interface FromProps {
    register: UseFormRegister<UserFormInput>;
    formState: { errors: FieldErrors<UserFormInput> };
    trigger: UseFormTrigger<UserFormInput>;
}