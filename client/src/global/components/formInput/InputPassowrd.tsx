import { Box, TextField, Typography } from "@mui/material";
import { FromProps } from "./propsType";
import { theme } from "../../styles/themes/themeModels";
import { useState } from "react";
import ShowPassowrd from "./ShowPassowrd";
import { userPasswordRegex } from "../../utils/regexs";

const InputPassword = (props: FromProps) => {
    const [showPassword, setShowPassword] = useState(false);

    return (
        <Box>
            <TextField
                margin="normal"
                fullWidth
                variant="outlined"
                label="password"
                type={showPassword ? "text" : "password"}
                {...props.register("password", { required: "password is required",
                    pattern: {
                    value: userPasswordRegex,
                    message: "Password must contain at least one uppercase letter, one lowercase letter, 7 digits, and one special character like @%&$."}})}
                InputProps={{ 
                    endAdornment: (
                        <ShowPassowrd showPassword={showPassword} setShowPassword={setShowPassword}/>
                    )
                }}
                onBlur={() => props.trigger("password")}
            />
            {props.formState.errors.password && <Typography color={theme.palette.error.light}>{props.formState.errors.password.message}</Typography>}
        </Box>
    )
}
export default InputPassword;



