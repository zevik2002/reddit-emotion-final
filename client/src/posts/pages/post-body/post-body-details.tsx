import { CardMedia, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { FC } from "react";
import { makeFirstLetterCapital } from "../../../global/utils/algoMethods";

type PostBodyProps = {
    body: string;
    image: string;
};

const PostBodyDetails: FC<PostBodyProps> = ({ body, image }) => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={6} lg={6}>
                <Box color="#969696" marginTop={2} marginLeft={2}>
                    <Typography>{makeFirstLetterCapital(body)}</Typography>
                </Box>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
                <CardMedia
                    sx={{
                        aspectRatio: 16 / 9,
                        objectFit: "cover",
                    }}
                    component="img"
                    image={image}></CardMedia>
            </Grid>
        </Grid>
    );
};
export default PostBodyDetails;
