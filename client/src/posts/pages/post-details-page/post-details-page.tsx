import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { Grid, Typography } from "@mui/material";
import useSendRequestToServer from "../../hooks/useSendRequestToServer";
import PostAuthorDetails from "../../components/post-author-details/post-author-details";
import PostBodyDetails from "../../components/post-body/post-body-details";
import Sentiment from "../../components/sentiment/sentiment";
import { parseDateString } from "../../../global/utils/algoMethods";
import PostCommentsPage from "./post-comments-page";
import Spinner from "../../../global/components/Spinner";
import { useErrorPage } from "../../../global/hooks/useErrorPage";
import PostInterface from "../../interfaces/PostInterface";
import { useAuth } from "../../../auth/AuthProvider";
import ROUTES from "../../../global/router/routes-model";
import { toastHandler } from "../../../global/components/toastify/toastHandler";

const PostDetailsPage = () => {
  const { category, postId } = useParams();
  const { user } = useAuth();
  const navigate = useNavigate();
  const {
    data: post,
    pending,
    error,
    handleGetPost,
  } = useSendRequestToServer<PostInterface>();
  useEffect(() => {
    if (category && postId) {
      handleGetPost({ category, postId });
    }
  }, []);

  useErrorPage(error ? { status: 403, message: error } : null);

  useEffect(() => {
    if (!user) {
      toastHandler("you must be logged in to view this page", "warn");
      navigate(ROUTES.LOGIN);
    }
  }, [user]);

  if (pending)
    return (
      <div style={{ height: "400px" }}>
        <Spinner />
      </div>
    );

  return (
    <>
      {post && (
        <>
          <Grid m={2} display="flex" flexDirection="column" alignItems="center">
            <Typography
              variant="h4"
              component="h1"
              color="white"
              marginTop="3vh"
              width="70%"
              flexWrap="wrap"
              mb={2}
            >
              {post.title}
            </Typography>
            <PostAuthorDetails
              author={post.author}
              created={parseDateString(new Date(post.created))}
            />
            <PostBodyDetails body={post.selftext} imageUrl={post.img} />
          </Grid>
          <Grid container justifyContent="center" mb={2}>
            {post.sentiment && <Sentiment sentiment={post.sentiment} />}
          </Grid>
          <Grid>
            <PostCommentsPage category={category} postId={postId} />
          </Grid>
        </>
      )}
    </>
  );
};
export default PostDetailsPage;
