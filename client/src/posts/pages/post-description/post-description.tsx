import { Box } from "@mui/system";
import { FC } from "react";

type PostDescriptionProps = {
    description: string;
    image: string;
};

const PostDescription: FC<PostDescriptionProps> = ({ description, image }) => {
    return (
        <Box>
            {description} {image}
        </Box>
    );
};
export default PostDescription;
