export type Title = {
  title: string;
  subTitle: string;
};

export const titlesForSelectPages: Title[] = [
  {
    title: "Select Category",
    subTitle:
      "Search for the category you would like to read and comment on related posts",
  },
  {
    title: "Select Subcategory",
    subTitle: "Select one of the following subcategories",
  },
  {
    title: "Filter Posts",
    subTitle: "Find the posts on the the topic that interests you",
  },
];
