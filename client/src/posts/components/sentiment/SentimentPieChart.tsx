import { PieChart } from "@mui/x-charts/PieChart";

function SentimentPieChart({
  comments,
}: {
  comments: { sentiment: string }[];
}) {
  const countSentiment = (sentiment: string) => {
    const arr = comments.filter((comment) => {
      return comment.sentiment === sentiment;
    });
    return { value: arr.length, label: sentiment };
  };
  const data = [
    { id: 0, ...countSentiment("NEGATIVE"), color: "red" },
    { id: 1, ...countSentiment("POSITIVE"), color: "green" },
    { id: 2, ...countSentiment("NEUTRAL"), color: "yellow" },
  ];
  return (
    <PieChart
      series={[
        {
          data,
        },
      ]}
      width={400}
      height={200}
    />
  );
}

export default SentimentPieChart;
