import { Box, Grid, Typography } from "@mui/material";
import { FC } from "react";
import { theme } from "../../../global/styles/themes/themeModels";

const getSentimentColor = (sentiment: string): string => {
  if (sentiment === "POSITIVE")
    return `${theme.palette.success.main}`;
  if (sentiment === "NEGATIVE")
    return `${theme.palette.warning.main}`;
  if (sentiment === "NEUTRAL")
    return `${theme.palette.secondary.main}`;
  return "";
};

const Sentiment: FC<{ sentiment: string, size?:number }> = ({ sentiment, size }) => {
  const fontSize = size
  return (
    <Grid data-testid="sentiment" item>
      <Box 
        px={1}
        py={0.4}
        display="flex"
        alignItems="center"
        justifyContent="center"
        bgcolor={getSentimentColor(sentiment)}
      >
        <Typography
          fontWeight={900}
          fontSize={fontSize}
          sx={{
            WebkitTextStrokeWidth: "1px",
            WebkitTextStrokeColor: `${theme.palette.primary.main}`,
          }}
        >
           {!fontSize?`semantic analysis:${sentiment}`: `${sentiment}`}
        </Typography>
      </Box>
    </Grid>
  );
};

export default Sentiment;
