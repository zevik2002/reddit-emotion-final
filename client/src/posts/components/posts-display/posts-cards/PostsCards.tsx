import { FC } from "react";
import PostCard from "./post-card/PostCard";
import { Grid } from "@mui/material";
import PostInterface from "../../../interfaces/PostInterface";

interface PostsCardsProps {
  cards: PostInterface[];
}

const PostsCards: FC<PostsCardsProps> = ({ cards }) => {
  return (
    <>
      <Grid container justifyContent="space-between" spacing={4} py={3}>
        {cards?.map((card) => {
          return (
            <PostCard
              key={card._id}
              imageUrl={card.img}
              title={card.title}
              selftext={card.selftext}
              category={card.category}
              postId={`${card._id}`}
            />
          );
        })}
      </Grid>{" "}
    </>
  );
};
export default PostsCards;
