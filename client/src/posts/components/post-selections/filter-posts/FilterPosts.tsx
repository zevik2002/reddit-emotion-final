import { FC, memo, useEffect, useState } from "react";
import { Container } from "@mui/material";
import PostsCards from "../../posts-display/posts-cards/PostsCards";
import useSendRequestToServer from "../../../hooks/useSendRequestToServer";
import FilterableInput from "../../../../global/components/FilterableInput";
import Spinner from "../../../../global/components/Spinner";
import PostInterface from "../../../interfaces/PostInterface";
import { toastHandler } from "../../../../global/components/toastify/toastHandler";

interface FilterPostsProps {
  handelSteps: React.Dispatch<React.SetStateAction<number>>;
  postCategory: {
    category: string;
    subCategory: string;
  };
}

const FilterPosts: FC<FilterPostsProps> = memo(function FilterPosts({ handelSteps, postCategory }) {
  const { category, subCategory } = postCategory;
  const { data: posts, pending, error, handelGetPostsByCategory } = useSendRequestToServer<PostInterface[]>();

  const [filteredPosts, setFilteredPosts] = useState<PostInterface[] | null>(posts);

  useEffect(() => {
    handelGetPostsByCategory(category, subCategory);
  }, []);

  useEffect(() => {
    if (posts && !filteredPosts) setFilteredPosts(posts);
    if (posts && posts.length < 1) {
      toastHandler(`Didn't find any posts in this category`, `error`);
      handelSteps(0);
    }
  }, [posts]);

  if (pending) return <Spinner />;
  if (error) return <div>Error...</div>;

  return (
    <Container>
      {posts && <FilterableInput array={posts} keyOfObject="title" onChange={setFilteredPosts} />}
      {filteredPosts && <PostsCards cards={filteredPosts} />}
    </Container>
  );
});

export default FilterPosts;
