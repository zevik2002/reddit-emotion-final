import { CardMedia, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { FC } from "react";
import { makeFirstLetterCapital } from "../../../global/utils/algoMethods";

type PostBodyProps = {
  body: string;
  imageUrl: string;
};

const PostBodyDetails: FC<PostBodyProps> = ({ body, imageUrl }) => {
  if (imageUrl === "self" || !imageUrl?.includes(".jpeg")) imageUrl = "/images/reddit-homePage-pic.webp";
  return (
    <Grid
      container
      spacing={2}
      display="flex"
      flexDirection="column"
      alignItems="center"
    >
      <Grid item xs={12} sm={12} md={8} lg={6}>
        <Box color="primary.light" marginTop={2} marginLeft={2}>
          <Typography>{makeFirstLetterCapital(body)}</Typography>
        </Box>
      </Grid>
      <Grid item xs={12} sm={12} md={6} lg={6} marginTop={2}>
        <CardMedia
          sx={{
            aspectRatio: 16 / 9,
            objectFit: "cover",
          }}
          component="img"
          image={imageUrl}
        ></CardMedia>
      </Grid>
    </Grid>
  );
};
export default PostBodyDetails;
